# K8S Demo App

Demo application for K8S.

## Configuration

Environment variables:

- `RUST_LOG` - logging level: info, debug, error, warn
- `USERNAME` - username
- `PASSWORD` - password

## API

See `API.md`.

## How to build image

See `BUILD.md`.

## Deploy app to K8S

See `DEPLOY-K8S.md`.