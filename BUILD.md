# Build

```shell
cargo build --release
```

## Build image

```shell
sudo docker image build -t k8s-demo-app:<VERSION>-<BUILD> .
```

Create tag for remote registry:

```shell
sudo docker image tag <TAG HASH> <YOUR-REGISTRY>/k8s-demo-app:<VERSION>-<BUILD>
sudo docker push <YOUR-REGISTRY>/k8s-demo-app:<VERSION>-<BUILD>
```

Example for GitLab:

```shell
sudo docker build --no-cache -t registry.gitlab.com/tinyops-k8s/k8s-demo-app:0.4.0
sudo docker push registry.gitlab.com/tinyops-k8s/k8s-demo-app:0.4.0
```
