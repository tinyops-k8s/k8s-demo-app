FROM rust:bullseye as build

COPY . .

RUN apt update && apt install binutils
RUN cargo build --release

FROM debian:bullseye as production

COPY --from=build /target/release/k8s-demo-app .

EXPOSE 8080

CMD ["./k8s-demo-app"]
