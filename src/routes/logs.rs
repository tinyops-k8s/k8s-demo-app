use actix_web::{get, Responder};
use chrono::{NaiveDateTime, Utc};
use serde::Serialize;

use crate::routes::utils::get_random_string;

/// Golang log row item
///
/// Example:
///
/// ```
/// {"log":"1:M 14 Mar 2023 08:05:55.130 * Background saving terminated with success\n","stream":"stdout","time":"2023-03-14T08:05:55.130250065Z"}
/// ```
#[derive(Serialize)]
pub struct GolangLogItem {
    pub time: NaiveDateTime,
    pub log: String,
    pub stream: String
}

#[get("/api/log/golang")]
pub async fn get_json_log_route() -> impl Responder {
    let row = get_random_log_row();

    match serde_json::to_string(&row) {
        Ok(json) => println!("{json}"),
        Err(e) => eprintln!("{}", e)
    }

    format!("log added")
}

fn get_random_log_row() -> GolangLogItem {
    let time: NaiveDateTime = Utc::now().naive_local();

    let log = format!("Log event {}", get_random_string());

    GolangLogItem {
        time,
        log,
        stream: "stdout".to_string()
    }
}
