use actix_web::{get, Responder};
use log::info;

use crate::APP_VERSION;

pub mod login;
pub mod metrics;
pub mod logs;
pub mod utils;

#[get("/")]
pub async fn get_index_route() -> impl Responder {
    info!("get index route");
    format!("K8s demo app")
}

#[get("/api/version")]
pub async fn get_version_route() -> impl Responder {
    info!("get app version");
    format!("{}", APP_VERSION)
}
