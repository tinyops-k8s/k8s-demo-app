use actix_web::{get, HttpResponse, Responder, web};
use log::info;
use rand::Rng;

use crate::APP_VERSION;

#[get("/metrics/{start}/{finish}")]
pub async fn get_random_metrics_route(path: web::Path<(u16, u16)>) -> impl Responder {
    let range: (u16, u16) = path.into_inner();
    let start: u16 = range.clone().0;
    let finish: u16 = range.clone().1;

    info!("get random metric from range '{}' to '{}'", start, finish);

    let mut rng = rand::thread_rng();
    let value: u16 = rng.gen_range(start..finish);

    let value_str = format!("{}", value);

    info!("random metric in range '{}' to '{}': {}", start, finish, value_str);

    HttpResponse::Ok().content_type("text/plain").body(value_str)
}

#[get("/metrics")]
pub async fn get_metrics_route() -> impl Responder {
    info!("get metrics");
    let metrics = format!("name k8s-demo-app\nversion {}", APP_VERSION);
    HttpResponse::Ok().content_type("text/plain").body(metrics)
}