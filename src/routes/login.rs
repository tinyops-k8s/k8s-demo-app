use std::env;

use actix_web::{get, HttpResponse, post, Responder, web};
use log::{error, info};
use serde::Deserialize;

#[get("/login")]
pub async fn get_login_page_route() -> impl Responder {
    info!("get login page");

    let form = " \
        <form method=\"post\" action=\"login\"> \
            <input type=\"text\" name=\"username\">
            <input type=\"password\" name=\"password\">
            <button type=\"submit\">Sign-in</button>
        </form>";

    HttpResponse::Ok().content_type("text/html").body(form)
}

#[derive(Deserialize)]
pub struct FormData {
    username: String,
    password: String,
}

#[post("/login")]
pub async fn login_route(form: web::Form<FormData>) -> impl Responder {
    match env::var("USERNAME") {
        Ok(username) => match env::var("PASSWORD") {
            Ok(password) => {
                if form.username == username {
                    info!("username - ok");
                    if form.password == password {
                        info!("auth ok");
                        HttpResponse::Ok().body("Auth ok")

                    } else {
                        error!("invalid password");
                        HttpResponse::Unauthorized().finish()
                    }
                } else {
                    error!("invalid username: '{}', expected: '{}'", form.username, username);
                    HttpResponse::Unauthorized().finish()
                }
            }
            Err(_) => {
                error!("env variable PASSWORD wasn't set");
                HttpResponse::InternalServerError().finish()
            }
        },
        Err(_) => {
            error!("env variable USERNAME wasn't set");
            HttpResponse::InternalServerError().finish()
        }
    }
}