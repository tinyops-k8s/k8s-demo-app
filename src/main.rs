use actix_web::{App, HttpServer};
use env_logger::{Builder, Target};
use log::info;

use crate::routes::{get_index_route, get_version_route};
use crate::routes::login::{get_login_page_route, login_route};
use crate::routes::logs::get_json_log_route;
use crate::routes::metrics::{get_metrics_route, get_random_metrics_route};

mod routes;

pub const APP_VERSION: &str = "0.4.0";

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let mut builder = Builder::from_default_env();
    builder.target(Target::Stdout);
    builder.init();

    info!("K8S DEMO APP v{}", APP_VERSION);
    info!("Listen port 8080");

    HttpServer::new(|| {
        App::new()
            .service(get_index_route)
            .service(get_version_route)
            .service(get_login_page_route)
            .service(login_route)
            .service(get_metrics_route)
            .service(get_random_metrics_route)
            .service(get_json_log_route)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
