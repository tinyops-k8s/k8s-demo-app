# Deploy to K8S

Edit `k8s/app.yaml` with your cluster domain etc.

### Prepare secrets

```shell
sudo docker login <private registry>
```

#### Option A: Vanilla secrets

Create secret:

```shell
kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/eugene/.docker/config.json --type=kubernetes.io/dockerconfigjson
```

#### Option B: Sealed secrets

```shell
kubectl create secret generic regcred --dry-run=client --from-file=.dockerconfigjson=/home/eugene/.docker/config.json --type=kubernetes.io/dockerconfigjson -o yaml > regcred.yml
 
kubeseal -o yaml < regcred.yml > sealed-regcred.yml

kubectl apply -f sealed-regcred.yml
```

Deploy app:

```shell
# Option A: for vanilla secrets
kubectl apply -f k8s/secret.yml

# Option B: for sealed secrets
kubectl apply -f k8s/sealed-secret.yml
kubectl apply -f k8s/app.yaml
```

Check app: http://k8s-demo-app.cluster.local