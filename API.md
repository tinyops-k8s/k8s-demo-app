# API

### Random metric in rage

GET `/metrics/10/291`

### Get login form

GET `/login`

### Login

POST `/login`

Fields:

- username
- password

### Get metrics

GET `/api/version`

### Print log row in Golang format (JSON)

GET `/api/log/golang`

Example:

```json
{"time":"2023-03-14T08:22:42.113560341","log":"Log event K1SXDmfXC","stream":"stdout"}
```


### Get version

GET `/api/version`
